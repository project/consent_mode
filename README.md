# Consent Mode

## Contents of this file

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

## Introduction

A simple module that allows users to insert a 'default' Google Consent Mode 
v2 compatible script into the site. The module can be enabled or disabled and
the default values for all consent choices can be set in the configuration.

It is important to note that this module adds supplementary functionality to 
cookie management modules. **If you do not have a cookie management module 
that can update these values then you should not make use of this module.**

For a full description of the module, visit the
[project page](https://www.drupal.org/project/consent_mode).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/consent_mode?categories=All).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. Visit:
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.


## Configuration

Configuration path `/admin/config/consent_mode` includes:

1. **Enable Google Consent mode**: This can be toggled to enable or disable
   the Consent Mode module from setting the default Google Consent Mode v2
   consent choices.
2. **Grant access to Ad Personalization**: If the toggle is selected then
   the default consent choice will be set to "granted" for "Ad 
   Personalization" if it is not selected it will be set to "denied".
3. **Grant access to Ad Storage**: If the toggle is selected then the 
   default consent choice will be set to "granted" for "Ad Storage" if it 
   is not selected it will be set to "denied".
4. **Grant access to Ad User Data**: If the toggle is selected then the
   default consent choice will be set to "granted" for "Ad User Data" if it
   is not selected it will be set to "denied".
5. **Grant access to Analytics Storage**: If the toggle is selected then the
   default consent choice will be set to "granted" for "Analytics Storage" if 
   it is not selected it will be set to "denied".
6. **Grant access to Functionality Storage**: If the toggle is selected then 
   the default consent choice will be set to "granted" for "Functionality 
   Storage" if it is not selected it will be set to "denied".
7. **Grant access to Personalization Storage**: If the toggle is selected then
   the default consent choice will be set to "granted" for "Personalization 
   Storage" if it is not selected it will be set to "denied".


## Maintainers

- [Roald Nel](https://www.drupal.org/u/roaldnel)
