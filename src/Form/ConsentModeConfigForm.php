<?php

namespace Drupal\consent_mode\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConsentModeConfigForm.
 */
class ConsentModeConfigForm extends ConfigFormBase {

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'consent_mode.consent_mode_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'consent_mode_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('consent_mode.consent_mode_config');

    $form['consent_mode_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Google Consent mode.'),
      '#default_value' => $config->get('consent_mode_enabled'),
    ];

    $form['ad_personalization'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Ad Personalization.'),
      '#default_value' => $config->get('ad_personalization'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['ad_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Ad Storage.'),
      '#default_value' => $config->get('ad_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['ad_user_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Ad User Data.'),
      '#default_value' => $config->get('ad_user_data'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['analytics_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Analytics Storage.'),
      '#default_value' => $config->get('analytics_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['functionality_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Functionality Storage.'),
      '#default_value' => $config->get('functionality_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['personalization_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Personalization Storage.'),
      '#default_value' => $config->get('personalization_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('consent_mode.consent_mode_config');
    $config->set('consent_mode_enabled', $form_state->getValue('consent_mode_enabled'))
      ->set('ad_personalization', $form_state->getValue('ad_personalization'))
      ->set('ad_storage', $form_state->getValue('ad_storage'))
      ->set('ad_user_data', $form_state->getValue('ad_user_data'))
      ->set('analytics_storage', $form_state->getValue('analytics_storage'))
      ->set('functionality_storage', $form_state->getValue('functionality_storage'))
      ->set('personalization_storage', $form_state->getValue('personalization_storage'));

    $config->save();
  }
}
