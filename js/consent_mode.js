window.dataLayer = window.dataLayer || [];
function gtag() {
  dataLayer.push(arguments);
}
gtag("consent", "default", {
  ad_personalization: drupalSettings.consent_mode.ad_personalization,
  ad_storage: drupalSettings.consent_mode.ad_storage,
  ad_user_data: drupalSettings.consent_mode.ad_user_data,
  analytics_storage: drupalSettings.consent_mode.analytics_storage,
  functionality_storage: drupalSettings.consent_mode.functionality_storage,
  personalization_storage: drupalSettings.consent_mode.personalization_storage,
  security_storage: "granted",
  wait_for_update: 500
});
gtag("set", "ads_data_redaction", true);
gtag("set", "url_passthrough", false);
